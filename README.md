# README #

Solution to the classic problem of making change.

Includes tests, arbitrary currency support, easy creation / consumption of moneys.

### Description ###

The problem of "making correct change" is a classic during S/W interviews.
The problem is as follows:

1. Provided an arbitrary set of coins ("Coins")
2. For any arbitrary amount or amounts.
3. Calculate and return the minimal subset of Coins totalling the exact amount.

**Example:**

*Coins provided*

* 10 quarters value of each = 25
*  1 dimes value of each = 10
*  5 nickel value of each = 5
*  5 pennies value of each = 1

*Change amount requested*

* 73

*Expected return*

* 2 quarters
* 1 dime
* 2 nickels
* 3 pennies


### How do I get set up? ###

* Requires CSharp 6
* Visual Studio 2015 solution included.
* Console App project has menu to run test

**example usage**

```
#!c#

var currency = new USCurrency();
var startWith = 2;   // start with how many of each denomination of coin
var bank = CoinBank.CreateWithBalance(currency, startWith);  // create a bank with 2 of each denomination of coin
var balance = bank.Balance;  // balance (total value) of coin deposited in bank, = 82 ( 2 each of quarters, dimes, nickels, pennies

var coinToDeposit = 10;  // coin of value 10, a "dime". Will be checked as valid against currency currency
var depositHowMany = 2; 
bank.DepositCoins(coinToDeposit, depositHowMany);    // add two dimes
balance = bank.Balance;     // we added 2 dimes so now 102

var withdrawalAmount = 52;
var change = bank.Withdraw(withdrawalAmount);
/*
change return as a CoinBank. In this case as follows

         Coin  |            Value  |            Count  |            Total
      -------------------------------------------------------------------
      Quarter  |               25  |                1  |               25
         Dime  |               10  |                2  |               20
       Nickel  |                5  |                1  |                5
        Penny  |                1  |                2  |                2
      -------------------------------------------------------------------
                            Total  |                6  |               52

*/
var changeAmountReturned = change.Balance;  // 52

...
```