﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace tdjr.MakeChange.Common
{
    public interface ICanBeDumped
    {
        IDump Dumper { get; set; }
        void Dump( string message = "" );

        Func<ICanBeDumped, string> ToDumpString();
    }
}