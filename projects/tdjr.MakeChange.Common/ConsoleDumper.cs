﻿using System;

namespace tdjr.MakeChange.Common
{
    public class ConsoleDumper : IDump
    {
        public void Dump( string message = "" )
        {
            Console.WriteLine(message);
        }

        public void Dump(ICanBeDumped toBeDumped, string message = "" )
        {
            if (toBeDumped == null) return;
            var func = toBeDumped.ToDumpString();
            var dumpStr = func != null ? func( toBeDumped ) : string.Empty;
            if (message != string.Empty) Console.WriteLine(message);
            Console.WriteLine(dumpStr);
        }
    }
}