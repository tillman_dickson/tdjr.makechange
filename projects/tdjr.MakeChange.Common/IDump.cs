﻿using System;

namespace tdjr.MakeChange.Common
{
    /// <summary>
    /// Provide interface for LinqPad type Dump() functionality
    /// </summary>
    public interface IDump
    {
        void Dump( string message = "" );

        void Dump(ICanBeDumped toBeDumped, string message = "");
    }
}