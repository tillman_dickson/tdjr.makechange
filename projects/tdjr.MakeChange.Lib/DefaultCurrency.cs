using System.Collections.Generic;
using System.Linq;

namespace tdjr.MakeChange.Lib
{
    public class DefaultCurrency : ICurrency
    {
        public string Name { get; set; }

        public List<KeyValuePair<int, string>> AvailableDenominations { get; set; }
        public DefaultCurrency()
        {
            Name = "Default Currency";
            AvailableDenominations = new List<KeyValuePair<int, string>>()
                                     {
                                         new KeyValuePair<int, string>(10, "Ten Piece"),
                                         new KeyValuePair<int, string>(5, "Five Piece" ),
                                         new KeyValuePair<int, string>(1, "Pfennig")
                                     };
        }

        public string GetAvailableDenominationName(int denominationValue)
        {
            try
            {
                var result = AvailableDenominations.FirstOrDefault(d => d.Key == denominationValue);
                return result.Value;
            }
            catch { return string.Empty; }
        }

    }
}