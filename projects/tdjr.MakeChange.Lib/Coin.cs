namespace tdjr.MakeChange.Lib
{
    /// <summary>
    /// a denominational value and available number of said denomination. 
    ///     NOTE: kinda redundant, Coin and Coins not clearly through through
    /// </summary>
    public class Coin
    {
        public int CoinValue { get; set; }
        public int CoinCount { get; set; }

        public Coin(int coinValue, int coinCount)
        {
            CoinValue = coinValue;
            CoinCount = coinCount;
        }

    }
}