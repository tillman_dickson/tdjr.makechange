﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tdjr.MakeChange.Common;

namespace tdjr.MakeChange.Lib
{
    public static class MakeChange
    {
        /// <summary>
        /// static entry point for making change
        /// </summary>
        /// <param name="currency"></param>
        /// <param name="bankVault"></param>
        /// <param name="changeFor"></param>
        /// <param name="dumper"></param>
        /// <returns></returns>
        public static int GetChange(ICurrency currency, IEnumerable<KeyValuePair<int, int>> bankVault, int changeFor, IDump dumper = null)
        {
            if (dumper == null) dumper = new NullDumper();

            var change = CoinBank.CreateEmptyCoinBank(currency, dumper);
            var coinBank = CoinBank.CreateEmptyCoinBank(currency, dumper);

            foreach (var denomination in bankVault)
            {
                coinBank.DepositCoins(denomination.Key, denomination.Value);
            }
            coinBank.Dump("          Starting bank");

            var remaining = CalcChangeRcsv(changeFor, coinBank, change);

            coinBank.Dump("          Ending bank");
            change.Dump("            Change provided");

            return remaining;
        }

        public static CoinBank GetChange(ICurrency currency, CoinBank bank, int changeFor, IDump dumper = null)
        {
            if (dumper == null) dumper = new NullDumper();

            var changeBank = CoinBank.CreateEmptyCoinBank(currency, dumper);
            var coinBank = bank; 
            
            coinBank.Dump("          Starting bank");

            var remaining = CalcChangeRcsv(changeFor, coinBank, changeBank);

            coinBank.Dump("          Ending bank");
            changeBank.Dump("            Change provided");

            return changeBank;
        }

        // recursive, returns amount outstanding. should normally be 0
        private static int CalcChangeRcsv(int amount, CoinBank coinBank, CoinBank change)
        {
            if (amount <= 0) return amount;

            coinBank.CoinsOnDeposit.ToList().ForEach(c =>
            {
                var coinsWithdrawn = c.WithDrawCoins(amount);
                if (coinsWithdrawn <= 0) return;
                change.DepositCoins(c.CoinValue, coinsWithdrawn);
                amount = amount - (c.CoinValue * coinsWithdrawn);
            });

            return amount;
        }

    }
}
