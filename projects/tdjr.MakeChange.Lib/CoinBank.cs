using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tdjr.MakeChange.Common;

namespace tdjr.MakeChange.Lib
{
    /// <summary>
    /// "bank" of Coins of a given Currency. Deposit/Withdraw
    /// </summary>
    public class CoinBank : ICanBeDumped
    {
        private List<Coins> CoinVault;

        public string BankTitle { get; set; }

        public ICurrency Currency { get; private set; }

        public IEnumerable<int> CoinDenominations => Currency.AvailableDenominations.Select(x => x.Key).OrderByDescending(d => d).Distinct();

        public IEnumerable<Coins> CoinsOnDeposit => CoinVault.Where(c => c.CoinCount > 0).OrderByDescending( c => c.CoinValue );

        public int Balance => CoinVault.Sum(c => c.CoinCount * c.CoinValue);

        public void DepositCoins(int coinValue, int howMany)
        {
            if (CoinDenominations.Contains(coinValue))
            {
                var coins = (from c in CoinVault
                             where c.CoinValue == coinValue
                             select c)
                    .FirstOrDefault();
                if (coins == null)
                    CoinVault.Add(new Coins(coinValue, howMany));
                else
                    coins.CoinCount += howMany;
            }
        }

        public CoinBank Withdraw( int amount )
        {
            return this.CalcChangeRcsv(amount);
        }

        #region Constructors

        public CoinBank(IDump dumper = null)
        {
            Currency = new DefaultCurrency();
            CoinVault = new List<Coins>();
            Dumper = dumper;
        }

        public CoinBank(ICurrency currency, List<Coins> coins, IDump dumper = null)
        {
            Currency = currency ?? new DefaultCurrency();
            CoinVault = coins ?? new List<Coins>();
            Dumper = dumper;
        }

        #endregion

        #region CoinBank Helpers

        // create a new CoinBank with no coins on deposit
        public static CoinBank CreateEmptyCoinBank(ICurrency currency, IDump dumper = null)
        {
            return CreateWithBalance(currency, 0, dumper);
        }

        // create a new CoinBank "howMany" coins  of each denomination in the currency
        public static CoinBank CreateWithBalance(ICurrency currency, int howMany, IDump dumper = null)
        {
            var denominations = currency.AvailableDenominations.Select(x => x.Key).Distinct().OrderBy(x => x);
            var coins = denominations.Select(coinValue => new Coins(coinValue, howMany)).ToList();
            return new CoinBank(currency, coins, dumper);
        }

        #endregion

        public IDump Dumper { get; set; }

        public void Dump(string message = "")
        {
            if (message != string.Empty && BankTitle == string.Empty)
                BankTitle = $"      {message}";
            Dumper?.Dump(this, message );
        }

        public Func<ICanBeDumped, string> ToDumpString()
        {
            return CoinBankDumpFormatter.DumpFunction;
        }

        
    }

    static class CoinBankExtension
    {
        public static CoinBank CalcChangeRcsv(this CoinBank bank, int amount)
        {
            // WARNING - not thread safe
            var resultBank = CoinBank.CreateEmptyCoinBank(bank.Currency, null);
            if (amount <= 0) return resultBank;

            bank.CoinsOnDeposit.ToList().ForEach(c =>
            {
                var coinsWithdrawn = c.WithDrawCoins(amount);
                if (coinsWithdrawn <= 0) return;
                resultBank.DepositCoins(c.CoinValue, coinsWithdrawn);
                amount = amount - (c.CoinValue * coinsWithdrawn);
            });

            return resultBank;
        }

    }

    static class CoinBankDumpFormatter
    {
        public static string DumpFunction(ICanBeDumped toBeDumped)
        {
            if (toBeDumped == null) return string.Empty;

            var bank = (CoinBank)toBeDumped;

            var result = new StringBuilder();

            var title = bank.BankTitle ?? " <no title> ";
            var titleLength = title.Length + 2;
            const int titleRowLength = 63;
            const string titleRowPrefix = "          ";
            var titleStars = (titleRowLength - titleRowPrefix.Length) / 2;
            var titleRow = titleRowPrefix +
                           "".PadLeft(titleStars, '*') +
                           $" {title} " +
                           "".PadLeft(titleStars + 1, '*');
            result.AppendLine(titleRow);

            result.AppendLine(CreateCoinRow(
                "  ",
                15,
                "  |  ",
                "Coin",
                "Value",
                "Count",
                "Balance"));
            result.AppendLine("          -------------------------------------------------------------------");
            var runningTotal = 0;
            foreach (var coins in bank.CoinsOnDeposit)
            {
                var coinValue = coins.CoinValue;
                var coinName = bank.Currency.GetAvailableDenominationName(coinValue);
                var coinCount = coins.CoinCount;
                var coinTotal = coinCount * coinValue;
                runningTotal += coinTotal;

                result.AppendLine(
                    CreateCoinRow(
                        "  ",
                        15,
                        "  |  ",
                        coinName,
                        coinValue.ToString(),
                        coinCount.ToString(),
                        coinTotal.ToString()));
            }
            var sumCoinValue = bank.CoinsOnDeposit.Select(c => c.CoinValue).Sum();
            var sumCoinCount = bank.CoinsOnDeposit.Select(c => c.CoinCount).Sum();
            var sumCoinTotal = runningTotal;
            result.AppendLine("          -------------------------------------------------------------------");
            result.AppendLine(
                CreateCoinRow(
                    "  ",
                    15,
                    "  |  ",
                    " ",
                    "Total",
                    sumCoinCount.ToString(),
                    sumCoinTotal.ToString(),
                    true));
            result.AppendLine(" ");

            return result.ToString();
        }



        private static string CreateCoinRow(
            string indent, int columnWidth,
            string separator,
            string coinName,
            string coinValue,
            string coinCount,
            string totalValue,
            bool skipFirstSeparator = false)
        {
            var result = new StringBuilder();
            result.Append(indent);

            result.Append(coinName.PadLeft(columnWidth));
            if (!skipFirstSeparator)
                result.Append(separator);
            else
                result.Append(' ', separator.Length);

            result.Append(coinValue.PadLeft(columnWidth));
            result.Append(separator);

            result.Append(coinCount.PadLeft(columnWidth));
            result.Append(separator);

            result.Append(totalValue.PadLeft(columnWidth));

            return result.ToString();
        }

    }
}