namespace tdjr.MakeChange.Lib
{
    /// <summary>
    /// coins of the same denomination
    /// </summary>
    public class Coins
    {
        public int CoinValue { get; set; }
        public int CoinCount { get; set; }

        // Withdraw coins valuing <= upToAmount.
        //
        // 	e.g. Withdraw exactly upToAmount
        //			CoinValue = 25, CounCount = 5, upToAmount = 75 will withdraw 3 Coins leaving CoinCount = 2
        //
        // 	e.g. Withdraw less than upToAmount due to coin denomination not an even multiple
        //			CoinValue = 25, CounCount = 5, upToAmount = 77 will withdraw 3 Coins leaving CoinCount = 2
        //
        // 	e.g. Will not overdraft 
        // 			CoinValue = 25, CounCount = 2, upToAmount = 75 will withdraw 2 Coins
        //
        // returns the number of coins withdrawn
        public int WithDrawCoins(int upToAmount)
        {
            if (upToAmount < CoinValue) return 0;

            var modVal = upToAmount % CoinValue;
            var withDrawCount = (upToAmount - modVal) / CoinValue;
            //if we don't have enough coins return all we have
            if (withDrawCount > CoinCount) withDrawCount = CoinCount;

            CoinCount = CoinCount - withDrawCount;

            return withDrawCount;
        }

        public void DepositCoins(int howMany) => CoinCount = howMany > 0 ? CoinCount + howMany : howMany;

        public int TotalValue => CoinValue * CoinCount;

        public Coins(int coinValue, int coinCount)
        {
            CoinValue = coinValue;
            CoinCount = coinCount;
        }
    }
}