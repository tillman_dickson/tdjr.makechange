using System.Collections.Generic;

namespace tdjr.MakeChange.Lib
{
    /// <summary>
    /// A currency, defined as a set of its available denominations, each denomination being a value and its name
    ///     e.g. 25, Quarter
    /// </summary>
    public interface ICurrency
    {
        string Name { get; set; }
        // Key = value of a coin, Value = name of the coin
        List<KeyValuePair<int, string>> AvailableDenominations { get; set; }
        // return the Currency's name for a give denomination's value
        string GetAvailableDenominationName(int denominationValue);
    }
}