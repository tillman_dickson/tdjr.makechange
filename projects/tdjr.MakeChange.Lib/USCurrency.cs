using System.Collections.Generic;
using System.Linq;

namespace tdjr.MakeChange.Lib
{
    public class USCurrency : ICurrency
    {
        public string Name { get; set; }
        public List<KeyValuePair<int, string>> AvailableDenominations { get; set; }
        public USCurrency()
        {
            Name = "US Currency";
            AvailableDenominations = new List<KeyValuePair<int, string>>()
                                     {
                                         new KeyValuePair<int, string>(25, "Quarter"),
                                         new KeyValuePair<int, string>(10, "Dime"),
                                         new KeyValuePair<int, string>(5, "Nickel" ),
                                         new KeyValuePair<int, string>(1, "Penny")
                                     };
        }

        public string GetAvailableDenominationName(int denominationValue)
        {
            try
            {
                var result = AvailableDenominations.FirstOrDefault(d => d.Key == denominationValue);
                return result.Value;
            }
            catch { return string.Empty; }
        }
    }
}