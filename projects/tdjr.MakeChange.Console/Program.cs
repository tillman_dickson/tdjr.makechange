﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tdjr.MakeChange.Test;

namespace tdjr.MakeChange
{
    class Program
    {
        static void Main(string[] args)
        {
            const string instructions =
                "Make Change Tests\n\n(a)\tAll tests\n(m)\tMake Change tests\n(b)\tBank Balance tests\n(Esc)\tExit\n\t(any other will exit)\n\nPlease select a test\n(For verbose test result use upper case letter)";
            Console.WriteLine(instructions);
            ConsoleKeyInfo userKey = Console.ReadKey();
            while (userKey.Key != ConsoleKey.Escape)
            {
                var inputChar =  userKey.KeyChar;
                switch (inputChar)
                {
                    case 'a':
                    case 'A':
                        Console.WriteLine("\n");
                        var allTests = new MakeChangeTest();
                        allTests.DoAllTests( char.IsUpper( inputChar ) );
                        break;

                    case 'b':
                    case 'B':
                        Console.WriteLine("\n");
                        var balanceTests = new MakeChangeTest();
                        var balResult = balanceTests.DoCreateBankTests( char.IsUpper( inputChar ) );
                        Console.WriteLine( balResult ? "PASS" : "FAIL" );
                        break;

                    case 'm':
                    case 'M':
                        Console.WriteLine("\n");
                        var makeChangeTests = new MakeChangeTest();
                        var mcResult = makeChangeTests.DoMakeChangeTests( char.IsUpper( inputChar ) );
                        Console.WriteLine( mcResult ? "PASS" : "FAIL" );
                        break;

                    default:
                        return;
                }
                Console.WriteLine("\n");
                Console.WriteLine("Press any key to continue.");
                if (Console.ReadKey().Key == ConsoleKey.Escape) return;

                Console.WriteLine("\n");
                Console.WriteLine(instructions);
                userKey = Console.ReadKey();

            }
        }
    }
    


}
