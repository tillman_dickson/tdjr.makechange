using System;
using System.Collections.Generic;
using tdjr.MakeChange.Common;
using tdjr.MakeChange.Lib;

namespace tdjr.MakeChange.Test
{
    public static class TestRunner
    {
        public static bool RunMakeChangeTest(
            string testName,
            Func<ICurrency, IEnumerable<KeyValuePair<int, int>>, int, IDump, int> fnTest,
            ICurrency currency, IEnumerable<KeyValuePair<int, int>> bankVault,
            int changeFor,
            int expectedRemainder = 0,
            IDump dumper = null)
        {

            if (dumper == null) dumper = new NullDumper();

            dumper.Dump("====== Begin Test ======");
            dumper.Dump($"{testName}");
            dumper.Dump($"       {currency.Name}");
            dumper.Dump($"       Calculate change for amount = {changeFor}");


            var remainingChange = fnTest(currency, bankVault, changeFor, dumper);
            var result = remainingChange.CompareChange(expectedRemainder);

            dumper.Dump($"       Outstanding change: {remainingChange}");
            dumper.Dump($"{result.BoolToPassFail()}");
            dumper.Dump("====================");
            dumper.Dump(" ");

            return result;
        }


        public static bool RunCreateBankTest(
            string testName,
            Func<ICurrency, IEnumerable<KeyValuePair<int, int>>, int, IDump, int> fnTest,
            ICurrency currency,
            IEnumerable<KeyValuePair<int, int>> bankVault,
            int expectedBankBalance = 0,
            IDump dumper = null )
        {
            if (dumper == null) dumper = new NullDumper();

            dumper.Dump("====== Begin Test ======");
            dumper.Dump($"{testName}");
            dumper.Dump($"       {currency.Name}");
            dumper.Dump($"       Create Bank with deposits");

            var balance = fnTest(currency, bankVault, expectedBankBalance, dumper);
            var result = balance.CompareChange(expectedBankBalance);

            dumper.Dump($"       Bank Balance: {balance}");
            dumper.Dump($"{result.BoolToPassFail()}");
            dumper.Dump("====================");
            dumper.Dump(" ");

            return result;
        }
    }
}