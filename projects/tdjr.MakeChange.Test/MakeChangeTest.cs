﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using tdjr.MakeChange.Common;
using tdjr.MakeChange.Lib;

namespace tdjr.MakeChange.Test
{
    [TestClass]
    public class MakeChangeTest
    {
        [TestMethod]
        public void DoAllTests(bool dumpDetailsToConsole = true)
        {
            var result = new StringBuilder(2);
            result.AppendLine( DoMakeChangeTests( dumpDetailsToConsole ).ToString() );
            result.AppendLine( DoCreateBankTests(dumpDetailsToConsole).ToString() );
            Console.WriteLine(result.ToString());
        }

        public bool DoMakeChangeTests(bool dumpDetailsToConsole = true)
        {
            var allPassed = true;

            IDump dumper;
            if (dumpDetailsToConsole)
                dumper = new ConsoleDumper();
            else 
                dumper = new NullDumper();

            var expectedResult = true; 
           var result = TestRunner.RunMakeChangeTest(
                    "Simple, should pass",
                    TestFunctions.MakeChangeTestFunc,
                    new USCurrency(),
                    new List<KeyValuePair<int, int>>() {
                            new KeyValuePair<int, int>(25, 5),
                            new KeyValuePair<int, int>(10, 5),
                            new KeyValuePair<int, int>(5, 5),
                            new KeyValuePair<int, int>(1, 5)
                        },
                    52,
                    0,
                    dumper
            ) == expectedResult;
            dumper.Dump( result.BoolToPassFail() );
            allPassed = allPassed && result;

            expectedResult = false;
            result = TestRunner.RunMakeChangeTest(
                    "Not enough pennies, should fail",
                    TestFunctions.MakeChangeTestFunc,
                    new USCurrency(),
                    new List<KeyValuePair<int, int>>() {
                        new KeyValuePair<int, int>(25, 5),
                        new KeyValuePair<int, int>(10, 5),
                        new KeyValuePair<int, int>(5, 5),
                        new KeyValuePair<int, int>(1, 0)
                    },
                    52,
                    0,
                    dumper
            ) == expectedResult;
            dumper.Dump( result.BoolToPassFail());
            allPassed = allPassed && result;

            result = TestRunner.RunMakeChangeTest(
                    "Make up for insufficient quarters, should pass",
                    TestFunctions.MakeChangeTestFunc,
                    new USCurrency(),
                    new List<KeyValuePair<int, int>>() {
                        new KeyValuePair<int, int>(25, 1),
                        new KeyValuePair<int, int>(10, 5),
                        new KeyValuePair<int, int>(5, 5),
                        new KeyValuePair<int, int>(1, 5)
                    },
                    52,
                    0,
                    dumper
            );
            dumper.Dump(result.BoolToPassFail());
            allPassed = allPassed && result;

            expectedResult = true;
            result = TestRunner.RunMakeChangeTest(
                    "Simple with Default currency, should pass. NOTE: adding quarters to starting bank which should be ignored",
                    TestFunctions.MakeChangeTestFunc, 
                    new DefaultCurrency(),
                    new List<KeyValuePair<int, int>>() {
                        new KeyValuePair<int, int>(25, 1),
                        new KeyValuePair<int, int>(10, 5),
                        new KeyValuePair<int, int>(5, 5),
                        new KeyValuePair<int, int>(1, 5)
                    },
                    52,
                    0,
                    dumper
            ) == expectedResult;
            dumper.Dump(result.BoolToPassFail());
            allPassed = allPassed && result;

            return allPassed;
        }

        public bool DoCreateBankTests( bool dumpDetailsToConsole = true )
        {
            var allPassed = true;

            IDump dumper;
            if (dumpDetailsToConsole)
                dumper = new ConsoleDumper();
            else
                dumper = new NullDumper();
            
            var expectedResult = true;
            var result = TestRunner.RunCreateBankTest(
                "Simple bank with 5 of each US Currency denomination, should pass",
                TestFunctions.CreateBankTestFunc,
                new USCurrency(),
                new List<KeyValuePair<int, int>>() {
                    new KeyValuePair<int, int>(25, 5),
                    new KeyValuePair<int, int>(10, 5),
                    new KeyValuePair<int, int>(5, 5),
                    new KeyValuePair<int, int>(1, 5)
                        },
                205,
                dumper
                ) == expectedResult;
            dumper.Dump(result.BoolToPassFail());
            allPassed = allPassed && result;

            expectedResult = true;
            result = TestRunner.RunCreateBankTest(
                "Simple bank with 0 of each US Currency denomination, should pass",
                TestFunctions.CreateBankTestFunc,
                new USCurrency(),
                new List<KeyValuePair<int, int>>() {
                    new KeyValuePair<int, int>(25, 0),
                    new KeyValuePair<int, int>(10, 0),
                    new KeyValuePair<int, int>(5, 0),
                    new KeyValuePair<int, int>(1, 0)
                        },
                0,
                dumper
                ) == expectedResult;
            dumper.Dump(result.BoolToPassFail());
            allPassed = allPassed && result;

            expectedResult = true;
            result = TestRunner.RunCreateBankTest(
                "Simple bank passing null in for Coin values, should pass",
                TestFunctions.CreateBankTestFunc,
                new USCurrency(),
                null,
                0,
                dumper
                ) == expectedResult;
            dumper.Dump(result.BoolToPassFail());
            allPassed = allPassed && result;


            return allPassed;
        }
    }
}
