using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tdjr.MakeChange.Common;
using tdjr.MakeChange.Lib;

namespace tdjr.MakeChange.Test
{
    /// <summary>
    /// Testing class for RunMakeChangeTest. Includes a number of extensions for formatting test output - particulary Dump() extenstion methods
    /// </summary>
    public static class TestFunctions
    {
        public static bool AllowDump = true;

        public static int MakeChangeTestFunc(
            //string testName,
            //Func<ICurrency, IEnumerable<KeyValuePair<int, int>>, int, int> fnTest,
            ICurrency currency, 
            IEnumerable<KeyValuePair<int, int>> bankVault,
            int changeFor,
            IDump dumper = null)
        {
            return Lib.MakeChange.GetChange(currency, bankVault, changeFor, dumper);
        }

        public static int CreateBankTestFunc(
            ICurrency currency, 
            IEnumerable<KeyValuePair<int, int>> bankVault,
            int expectedRemainder = 0,
            IDump dumper = null )
        {
            var coinBank = CoinBank.CreateEmptyCoinBank(currency, dumper);

            if (bankVault != null)
                foreach (var denomination in bankVault)
                {
                    coinBank.DepositCoins(denomination.Key, denomination.Value);
                }
            coinBank.Dump("Coin bank");

            return coinBank.Balance;
        }
        

        #region Extension Methods for logging and log entry formatting

            // send a string to the console
        public static void Dump(this string self)
        {
            if (AllowDump)
                Console.WriteLine(self);
        }
        

        public static bool CompareChange(this int self, int compareTo) => self.CompareTo(compareTo) == 0;

        // convert bool true/false to string of "PASS"/"FAIL"
        public static string BoolToPassFail(this bool self) => self ? "PASS" : "FAIL";

        // returns a string left-padded to a requested total length.
        // Optionally appends a string (postFix) to the resulting padded string 
        public static string PadLeftTo(
            this string self,
            int toLength,
            char padChar = ' ',
            string postFix = "")
        {
            var padLength = toLength - self.Length;
            if (padLength > 0)
                return self.PadLeft(padLength, padChar) + postFix;
            else
                return self.ToString() + postFix;
        }

        #endregion

    }
}